import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;


public class Hand {
	private Card [] bridgeCards;
	private int [] suitCount;
	private int HCP;
	private int[] suitHCP;
	
	private static final String[] strBalance = { "Balanced", "Semi-Balanced", "Unbalanced" };
	private static final String[] strSuiter = { "", "Single Suiter", "Two Suiter", "Three Suiter" };
	private static final String[] strStrength = { "Weak", "Intermediate", "Strong", "Very Strong" };
	private static final String[] strGrade = { "A", "B", "C", "D" };
	public  static final char[] suits = { 'S', 'H', 'D', 'C' };
	
	private static final int Balanced = 0;
	private static final int semiBalanced = 1;
	private static final int unBalanced = 2;
	
	private static final int SingleSuiter = 1;
	private static final int TwoSuiter = 2;
	private static final int ThreeSuiter = 3;
	
	private static final int Major = 0;
	private static final int Minor = 1;
	private static final int Black = 2;
	private static final int Red = 3;
	
	private static final int Bad = 0;
	private static final int Weak = 1;
	private static final int Opening = 2;
	private static final int Strong = 3;
	private static final int VeryStrong = 4;
	
	public Hand(Card[] cards){
		bridgeCards = new Card[13];
		suitCount = new int[4];
		suitHCP = new int[4];
		bridgeCards = cards;
		countSuits();
	}
	
	private void countSuits() {
		for(int i = 0; i < bridgeCards.length; i++){
		//System.out.println(i);
			Card c = bridgeCards[i];
			char suit = c.getSuit();
			int pip = c.getpip();
			suitCount[suitToInt(suit)]++;
			if(pip > 10){
				suitHCP[suitToInt(suit)] += pip - 10;
				HCP += pip - 10;
			}
		}
	}
	
	public int suiter(){
		int suit = 0;
		int count5 = countPlus(5);
		int count4 = countPlus(4);
		if(count5 == 1 && count4 == 1) return SingleSuiter;
		if(count5 == 1 && count4 == 2) return TwoSuiter;
		if(count4 == 3) return ThreeSuiter;
		return suit;
	}
	
	public boolean isMajor(){
		if(suiter() == TwoSuiter){
			boolean[] suits4 = suitPlus(4);
			boolean[] suits5 = suitPlus(5);
			if(suits4[0] && suits4[1]){
				if(suits5[0] || suits5[1])
					return true;
			}
		}
		return false;
	}
	
	public int typeTS(){
		if(suiter() == TwoSuiter){
			if(isValid(suitToInt('S'), suitToInt('H'))) return Major;
			else if (isValid(suitToInt('D'), suitToInt('C'))) return Minor;
			else if (isValid(suitToInt('H'), suitToInt('D'))) return Red;
			else if (isValid(suitToInt('S'), suitToInt('C'))) return Black;
		}
		return -1;
	}
	
	public boolean isValid(int i, int j){
			boolean[] suits4 = suitPlus(4);
			boolean[] suits5 = suitPlus(5);
			if(suits4[i] && suits4[i]){
				if(suits5[j] || suits5[j])
					return true;
			}
		return false;
	}
	
	public boolean isPreEmpt(){
		return false;
	}
	
	public int balance(){
		int doublets = count(2);
		int singlets = count(1);
		int voids = count(0);
		if (voids > 0 || singlets > 0)
			return unBalanced;
		else if (doublets >= 2)
			return semiBalanced;
		else
			return Balanced;
	}
	
	private int count(int n){
		int count = 0;
		for (int i = 0; i < suitCount.length; i++){
			if (suitCount[i] == n){
				count++;
			}
		}
		return count;
	}
	
	
	public int strength(){
		if(HCP > 0 && HCP <= 5)
			return Bad;
		else if(HCP >= 6 && HCP <= 10 )
			return Weak;
		else if (HCP >= 11 && HCP <= 15)
			return Opening;
		else if (HCP >= 16 && HCP <= 19)
			return Strong;
		else if (HCP >= 20)
			return VeryStrong;
		return -1;
	}
	
	public boolean isNoTrump(){
		int bal = balance();
		return ((bal == Balanced) && (HCP >= 15 && HCP <= 21));
	}
	
	public boolean isGameForce(){
		int bal = balance();
		int suit = suiter();
		return ((bal == Balanced && HCP >= 22) || (suit == TwoSuiter && HCP >= 20));
	}
	
	public String evaluate(){
		return strStrength[strength()] + " " + strBalance[balance()] + " " + strSuiter[suiter()] + " - " + grade();
	}
	
	private int countPlus(int n){
		int count = 0;
		for (int i = 0; i < suitCount.length; i++){
			if (suitCount[i] >= n){
				count++;
			}
		}
		return count;
	}
	
	private boolean[] suitPlus(int n){
		boolean[] suits = new boolean[4];
		Arrays.fill(suits, false);
		for (int i = 0; i < suitCount.length; i++){
			if (suitCount[i] >= n){
				suits[i] = true;
			}
		}
		return suits;
	}
	
	public String grade(){
		String gradestr = strGrade[strength()];
		if(countPlus(5) >= 2 && strength() > 0) gradestr += "+";
		return gradestr;
	}
	
	public static int charToint(String c){
		if(c.equals("A")) return 14;
		if(c.equals("K")) return 13;
		if(c.equals("Q")) return 12;
		if(c.equals("J")) return 11;
		if(c.equals("T")) return 10;
		return Integer.parseInt(c);
 	}
	
	private int suitToInt(char c){
		if(c == 'S') return 0;
		if(c == 'H') return 1;
		if(c == 'D') return 2;
		if(c == 'C') return 3;
		return -1;
	}
	
	public String toString(){
		String str = "";
		for(int i = 0; i < bridgeCards.length; i++){
			str += bridgeCards[i] + " ";
		}
		return str;
	}
	
	public int getHCP() {
		return HCP;
	}
	
	public static void main(String args[]) throws IOException{
		BufferedReader r = new BufferedReader(new FileReader("card-deals.txt"));
		String str = "";
		while((str = r.readLine()) != null){
			Card[] hand = new Card[13];
			String[] cards = str.split(" ");
			//System.out.println(Arrays.toString(cards));
			int index = 0;
			for(int i = 0; i < cards.length; i++){
				for(int j = 0; j < cards[i].length(); j++){
					int pip = Hand.charToint(cards[i].charAt(j) + "");
					char suit = suits[i];
					//System.out.println("" + suit + pip);
					hand[index++] = new Card(suit, pip);
				}
			}
		//	System.out.println(Arrays.toString(hand));
			Hand h = new Hand(hand);
			System.out.println(h);
			System.out.println(h.evaluate());
			System.out.println();
		}
		r.close();
	}

}
