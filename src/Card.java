public class Card implements Comparable<Card>{
    private static final String PIPS = "**23456789TJQKA";
    
    private char suit;
    private Integer pip;
    private String name;
    
    public Card(char suit, Integer pip){
    	this.suit = suit;
    	this.pip = pip;
    	this.name = "" + suit + PIPS.charAt(pip);
    }
    
    public String toString() {
    	return name;
    }
	
    public char getSuit() {
    	return suit;
    }
    
    public Integer getpip() {
    	return pip;
    }
    
    public boolean equals(Object o) {
		if (o instanceof Card) 
		    return (this.getpip() == ((Card)o).getpip()) && (this.getSuit() == ((Card)o).getSuit());
		else
		    return false;
	}
    
    public int compareTo(Card c){
		if(this.pip == c.getpip()) 		return 0;
		else if(this.pip < c.getpip()) 	return -1;
		else if(this.pip > c.getpip()) 	return 1;
		return -1;
    }
}
