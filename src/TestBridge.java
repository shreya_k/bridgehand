import static org.junit.Assert.*;

import org.junit.Test;

public class TestBridge {

	Card[] c = null;

	public void Initialize() {

		c = new Card[13];
		c[0] = new Card('H', 1);
		c[1] = new Card('C', 4);
		c[2] = new Card('S', 9);
		c[3] = new Card('C', 7);
		c[4] = new Card('H', 5);
		c[5] = new Card('S', 5);
		c[6] = new Card('C', 7);
		c[7] = new Card('H', 8);
		c[8] = new Card('S', 9);
		c[9] = new Card('C', 10);
		c[10] = new Card('C', 11);
		c[11] = new Card('C', 12);
		c[12] = new Card('C', 13);

	}

	@Test
	public void testBalanced() {
		Hand h = new Hand(c);
		assertEquals(0, h.balance());
	}

	@Test
	public void testunBalanced() {
		Hand h = new Hand(c);
		assertEquals(2, h.balance());
	}

	@Test
	public void testsemiBalanced() {
		Hand h = new Hand(c);
		assertEquals(1, h.balance());
	}

	@Test
	public void testsingleSuiter() {
		Hand h = new Hand(c);
		assertEquals(1, h.suiter());
		
	}

	@Test
	public void testtwoSuiter() {
		Hand h = new Hand(c);
		assertEquals(2, h.suiter());
		
	}
	@Test
	public void testthreeSuiter() {
		Hand h = new Hand(c);
		assertEquals(3, h.suiter());
		
	}
}
